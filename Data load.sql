INSERT INTO dbo.FrequencyType (Value, DisplayOrder)
	VALUES ('Hourly', 1)
INSERT INTO dbo.FrequencyType (Value, DisplayOrder)
	VALUES ('Daily', 2)
INSERT INTO dbo.FrequencyType (Value, DisplayOrder)	
	VALUES ('Weekly', 3)

INSERT INTO dbo.StatusType (Value, DisplayOrder)
	VALUES ('Passed', 1)
INSERT INTO dbo.StatusType (Value, DisplayOrder)	
	VALUES ('PartiallyFailed', 2)
INSERT INTO dbo.StatusType (Value, DisplayOrder)	
	VALUES ('Failed', 3)
INSERT INTO dbo.StatusType (Value, DisplayOrder)	
	VALUES ('Skipped', 4)


DBCC CHECKIDENT ('dbo.Accounts', RESEED, 0)
DBCC CHECKIDENT ('dbo.Checks', RESEED, 0)
DBCC CHECKIDENT ('dbo.Steps', RESEED, 0)


DECLARE @UserId UNIQUEIDENTIFIER 
SELECT TOP 1 @UserId = [UserId] FROM [dbo].[aspnet_Users]
	WHERE UserName = 'lholman'
SELECT @UserId

INSERT INTO dbo.Accounts (Name, HostName, PrimaryUserId, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES('Datum Generics Ltd', 'DatumGenerics', @UserId, @UserId, null, null)
	
INSERT INTO dbo.UsersInAccounts (UserId, AccountId)
	VALUES(@UserId, 1)	

INSERT INTO dbo.Checks (AccountId, Name, [Description], IsActive, FrequencyTypeValue, FrequencyComponent, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (1, 'Database Checks', NULL, 1, 'Daily', '9999-12-31 08:30:00.000', @UserId, null, null)
INSERT INTO dbo.Steps (CheckId, [Description], IsInstructionalOnly, IsActive, RunOrder, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (1, 'RDP on to INF-VM-MON-01 and navigate to C:\Scripts', 1, 1, 1, @UserId,  null, null)
INSERT INTO dbo.Steps (CheckId, [Description], IsInstructionalOnly, IsActive, RunOrder, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (1, 'Run C:\Scripts\dba_backupstats and ensure todays date is set against each database', 0, 1, 2, @UserId, null, null)

INSERT INTO dbo.Checks (AccountId, Name, [Description], IsActive, FrequencyTypeValue, FrequencyComponent, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (1, 'File System Checks', NULL, 1, 'Daily', '9999-12-31 08:45:00.000', @UserId, null, null)
INSERT INTO dbo.Steps (CheckId, [Description], IsInstructionalOnly, IsActive, RunOrder, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (2, 'RDP on to INF-VM-MON-01 and navigate to C:\Scripts', 1, 1, 1, @UserId, null, null)	 

INSERT INTO dbo.Checks (AccountId, Name, [Description], IsActive, FrequencyTypeValue, FrequencyComponent, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (1, 'VPN Connectivity', NULL, 1, 'Daily', '9999-12-31 08:45:00.000', @UserId, null, null)
INSERT INTO dbo.Steps (CheckId, [Description], IsInstructionalOnly, IsActive, RunOrder, CreatedBy, UpdatedOnUtc, UpdatedBy)
	VALUES (3, 'Connect to VPN', 1, 1, 1, @UserId, null, null)	

SELECT * FROM dbo.Accounts	 
SELECT * FROM dbo.Checks	 
SELECT * FROM dbo.Steps
