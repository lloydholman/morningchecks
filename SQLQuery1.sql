SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

/* ASP.NET Application Services Schema */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Status]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StatusType](
	[Value] [nvarchar](32) NOT NULL,
	[DisplayOrder] INT NOT NULL,
	CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FrequencyType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FrequencyType](
	[Value] [nvarchar](32) NOT NULL,
	[DisplayOrder] INT NOT NULL,
	CONSTRAINT [PK_FrequencyType] PRIMARY KEY CLUSTERED 
(
	[Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Accounts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[HostName] [nvarchar] (128) NOT NULL,
	[PrimaryUserId] [uniqueidentifier] NOT NULL,
	[CreatedOnUtc] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[UpdatedOnUtc] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,	
	CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[Accounts] WITH CHECK ADD CONSTRAINT [FK_Accounts_aspnet_Users] FOREIGN KEY([PrimaryUserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Accounts] ADD CONSTRAINT [DF_Accounts_CreatedOnUtc] DEFAULT GETDATE() FOR [CreatedOnUtc]
GO
ALTER TABLE [dbo].[Accounts]  WITH CHECK ADD CONSTRAINT [FK_Accounts_CreatedBy_aspnet_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Accounts] CHECK CONSTRAINT [FK_Accounts_CreatedBy_aspnet_Users]
GO	
ALTER TABLE [dbo].[Accounts]  WITH CHECK ADD CONSTRAINT [FK_Accounts_UpdatedBy_aspnet_Users] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Accounts] CHECK CONSTRAINT [FK_Accounts_UpdatedBy_aspnet_Users]
GO		

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersInAccounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UsersInAccounts](
	[UserId] [uniqueidentifier] NOT NULL,
	[AccountId] [int] NOT NULL,
	CONSTRAINT [PK_UsersInAccounts] PRIMARY KEY CLUSTERED 
(
	[AccountId],[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[UsersInAccounts] 
WITH CHECK ADD CONSTRAINT FK_UsersInAccounts_aspnet_Users FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersInAccounts] 
WITH CHECK ADD CONSTRAINT FK_UsersInAccounts_Accounts FOREIGN KEY([AccountId])
REFERENCES [dbo].[Accounts] ([Id])
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Checks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Checks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,	
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[IsActive] [bit] NOT NULL,
	[FrequencyTypeValue] [nvarchar](32) NOT NULL,
	[FrequencyComponent] [datetime] NOT NULL,	
	[CreatedOnUtc] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[UpdatedOnUtc] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[Checks] WITH CHECK ADD CONSTRAINT [FK_Checks_FrequencyType] FOREIGN KEY([FrequencyTypeValue])
REFERENCES [dbo].[FrequencyType] ([Value])
GO
ALTER TABLE [dbo].[Checks] WITH CHECK ADD CONSTRAINT [FK_Checks_Accounts] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Accounts] ([Id])
GO
ALTER TABLE [dbo].[Checks] ADD CONSTRAINT [DF_Checks_CreatedOnUtc] DEFAULT GETDATE() FOR [CreatedOnUtc]
GO
ALTER TABLE [dbo].[Checks]  WITH CHECK ADD CONSTRAINT [FK_Checks_CreatedBy_aspnet_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Checks] CHECK CONSTRAINT [FK_Checks_CreatedBy_aspnet_Users]
GO	
ALTER TABLE [dbo].[Checks]  WITH CHECK ADD CONSTRAINT [FK_Checks_UpdatedBy_aspnet_Users] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Checks] CHECK CONSTRAINT [FK_Checks_UpdatedBy_aspnet_Users]
GO	



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Steps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Steps](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CheckId] [int] NOT NULL,
	[Description] [nvarchar] (1025) NOT NULL,
	[IsInstructionalOnly] BIT NOT NULL,
	[IsActive] [bit] NOT NULL,	
	[RunOrder] [int] NOT NULL,
	[CreatedOnUtc] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[UpdatedOnUtc] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,	
	CONSTRAINT [PK_Steps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[Steps]  
WITH CHECK ADD CONSTRAINT [FK_Steps_Checks] FOREIGN KEY([CheckId])
REFERENCES [dbo].[Checks] ([Id])
GO
ALTER TABLE [dbo].[Steps] ADD CONSTRAINT [DF_Steps_CreatedOnUtc] DEFAULT GETDATE() FOR [CreatedOnUtc]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChecksRunLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChecksRunLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[Name] [nvarchar] (128) NOT NULL,
	[Description] [nvarchar] (1025) NULL,
	[IsInstructionalOnly] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[StatusTypeValue] [nvarchar](32) NOT NULL,	
	[FrequencyTypeValue] [nvarchar](32) NOT NULL,
	[FrequencyTypeDateTimeValue] [datetime] NOT NULL,
	[Notes] [nvarchar] (1025) NULL,
	[StartedRun] [datetime] NOT NULL,
	[FinishedRun] [datetime] NOT NULL,
	[CreatedOnUtc] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[UpdatedOnUtc] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	CONSTRAINT [PK_ChecksRunLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[ChecksRunLog] WITH CHECK ADD CONSTRAINT [FK_ChecksRunLog_Status] FOREIGN KEY([StatusTypeValue])
REFERENCES [dbo].[StatusType] ([Value])
GO
ALTER TABLE [dbo].[ChecksRunLog] WITH CHECK ADD CONSTRAINT [FK_ChecksRunLog_FrequencyType] FOREIGN KEY([FrequencyTypeValue])
REFERENCES [dbo].[FrequencyType] ([Value])
GO
ALTER TABLE [dbo].[ChecksRunLog] ADD CONSTRAINT DF_ChecksRunLog_CreatedOnUtc DEFAULT GETDATE() FOR [CreatedOnUtc]
GO
ALTER TABLE [dbo].[ChecksRunLog]  WITH CHECK ADD CONSTRAINT [FK_ChecksRunLog_CreatedBy_aspnet_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[ChecksRunLog] CHECK CONSTRAINT [FK_ChecksRunLog_CreatedBy_aspnet_Users]
GO	
ALTER TABLE [dbo].[ChecksRunLog]  WITH CHECK ADD CONSTRAINT [FK_ChecksRunLog_UpdatedBy_aspnet_Users] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[ChecksRunLog] CHECK CONSTRAINT [FK_ChecksRunLog_UpdatedBy_aspnet_Users]
GO	


ALTER TABLE [dbo].[aspnet_Users] ADD CONSTRAINT UC_aspnetUsers_UserName UNIQUE NONCLUSTERED (UserName)
GO

SET ANSI_PADDING OFF
GO




		





		
	
		






