﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Massive;

namespace MorningChecks.Models
{
    public class Checks : DynamicModel
    {
        public Checks() : base("ApplicationServices")
        {   
            PrimaryKeyField = "Id";
        }
    }

    public class Steps : DynamicModel
    {
        public Steps() : base("ApplicationServices")
        {
            PrimaryKeyField = "Id";
        }
    }

}