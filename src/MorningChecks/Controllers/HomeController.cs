﻿using System.Collections;
using System.Web.Mvc;
using MorningChecks.Models;

namespace MorningChecks.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "";
            
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
