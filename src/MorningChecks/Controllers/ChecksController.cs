﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Mvc;
using MorningChecks.Models;
using MorningChecks.Views.Checks;

namespace MorningChecks.Controllers
{
    public class ChecksController : Controller
    {
        // GET: /Checks/
        [Authorize]
        public ActionResult Index()
        {
            var model = new ChecksSummaryViewModel
                            {
                                AccountId = int.Parse(Session["AccountId"].ToString()),
                                UserId = Guid.Parse(Session["UserId"].ToString())
                            };

            dynamic tbl = new Checks();
            var checks = tbl.Find(AccountId: model.AccountId );
            dynamic tblSteps = new Steps();

            foreach (var check in checks)
            {
                CheckViewModel checkViewModel = new CheckViewModel { Check = check };

                IEnumerable<dynamic> steps = tblSteps.Find(IsActive: true, CheckId: check.Id, OrderBy: "RunOrder ASC");
                checkViewModel.Steps = steps;

                model.Checks.Add(checkViewModel);
            }



            return View(model);
        }

        // GET: /Checks/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: /Checks/Create
        public ActionResult Create()
        {
            var viewModel = new CheckViewModel {Check = new ExpandoObject()};
            viewModel.Check.AccountId = int.Parse(Session["AccountId"].ToString());
            viewModel.Check.CreatedBy = Guid.Parse(Session["UserId"].ToString());
            return PartialView("_AddCheck", viewModel);
            return View();
        } 

        // POST: /Checks/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                dynamic tbl = new Checks();
                var newId = tbl.Insert(Request.Form);
                return RedirectToAction("Index", "Checks");
            }
            catch
            {
                return View();
            }
        }
        
        // GET: /Checks/Edit/5
        public ActionResult Edit(int id)
        {
            dynamic tbl = new Checks();
            var check = tbl.First(Id: id);

            var viewModel = new CheckViewModel {Check = check};
            return PartialView("_EditCheck", viewModel);
        }

        // POST: /Checks/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                dynamic tbl = new Checks();
                tbl.Update(Request.Form, id);
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Checks/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: /Checks/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
