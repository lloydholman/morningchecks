using System.Collections.Generic;

namespace MorningChecks.Views.Checks
{
    public class CheckViewModel
    {
        public dynamic Check {get; set;}
        public IEnumerable<object> Steps { get; set; }

    }
}