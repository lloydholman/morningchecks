﻿using System;
using System.Collections.Generic;

namespace MorningChecks.Views.Checks
{
    public class ChecksSummaryViewModel
    {
        public List<CheckViewModel> Checks {get; set;}
        public int AccountId { get; set; }
        public Guid UserId { get; set; }
        
        public ChecksSummaryViewModel()
        {
            Checks = new List<CheckViewModel>();
        }
    }


}